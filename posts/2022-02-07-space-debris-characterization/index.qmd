---
title: Space Debris Characterization Using Machine Learning Methods
date: 2022-02-07
# date-modified: 2024-11-13
author:
  - name: Anson Biggs
    url: https://ansonbiggs.com
    roles:
      - "Data curation"
      - "Formal analysis"
      - "Investigation"
      - "Methodology"
      - "Software"
      - "Validation"
      - "Visualization"
      - "Writing - original draft"
      - "Writing - review & editing"
    affiliation: 
      - name: Embry-Riddle Aeronautical University
        city: Prescott
        state: AZ
        url: https://erau.edu
  - name: Ana Bader-Elenes
    roles: 
    - "Data curation"
    - "Investigation"
    - "Validation"
    - "Writing - review & editing"
    affiliation: Embry-Riddle Aeronautical University
  - name: Dr. Mehran Andalibi
    affiliation: Associate Professor of Mechanical Engineering, Embry-Riddle Aeronautical University, Prescott, AZ
    roles: Advisor
  - name: Dr. Ron Madler
    affiliation: Dean of the College of Engineering, Embry-Riddle Aeronautical University, Prescott, AZ
    roles: Advisor
abstract: >
    Orbital debris is a form of pollution that is growing at an exponential pace and puts current and future space infrastructure at risk. Satellites are critical to military, commercial, and civil operations. Unfortunately, the space they occupy is increasingly becoming more crowded and dangerous, potentially leading to a cascade event that could turn orbit around the Earth into an unusable wasteland for decades proper mitigation is not introduced. Unfortunately, existing models employed by NASA rely on a dataset created from 2D images and are missing many crucial features required for correctly modeling the space debris environment. Our approach uses high-resolution 3D scanning to fully capture the geometry of a piece of debris and allow a more advanced analysis of each piece. This approach, coupled with machine learning methods, will allow advances to the current cutting edge. Physical and photograph-based measurements are time-consuming, hard to replicate, and lack precision. 3D scanning allows much more advanced and accurate analysis of each debris sample, focusing on properties such as moment of inertia, cross-section, and drag. With these additional properties, we stand to substantially increase our understanding of the space debris environment through advanced characterization of each piece of debris. Once the characteristics of space debris are more thoroughly understood, we can begin mitigating the creation and danger of future space debris by implementing improved satellite construction methods and more advanced debris avoidance measures. 
description: |
    Research proposal detailing the growing threat of orbital debris and its significant risk to space infrastructure.
description-meta: |
    Researching orbital debris using 3D scanning and machine learning to improve current models and inform mitigation strategies. Learn how this approach enhances our understanding of space debris and its impact on space infrastructure.
funding: "This work was funded by ERAU URI Ignite Award"
other-links:
    - text: URI Discovery Day Post
      href: https://commons.erau.edu/pr-discovery-day/2022/presentations/5/
categories:
  - Orbital Debris
  - Machine Learning
  - Space
  - University
  - Engineering

bibliography: references.bib
csl: diabetologia.csl
---

:::{.callout-note}
## Important Note

This is a reposting of my research proposal since my Univeristy only posted the abstract and a very low quality image. I removed any information that wasn't written by me, or anything that could be considered sensitive. 

:::

## Project Summary

There are currently approximately 23,522 debris objects 10 centimeters in size or larger tracked by NASA, an estimated 500,000 objects 1 centimeter in size or larger, and upwards of 100 million debris objects at least 1 millimeter in size [@noauthor_space_2022]. Furthermore, the average relative velocity of objects in Low Earth Orbit is ten km/s [@wertz_space_2011], which means collisions with even tiny pieces of debris can end a space mission and create more debris. These factors could combine and lead to a vicious cycle where debris causes more debris, potentially creating debris faster than atmospheric drag could remove it and, in an extreme case blocking access to space for decades [@kessler_collision_1978]. Essential factors in predicting the damage due to collision with space debris are its mass, density, speed, and shape. As a result, developing models to predict the orbits of debris and categorizing them based on their shape characteristics is a growing focus for the US and allied space agencies [@orbital_debris_research_and_developmentinteragency_working_group_2021_2021]. These models are used to maneuver around the upcoming debris or assess the potential damage due to collisions. 
Current studies have the following shortcomings. Although they only utilize a few shape characteristic factors for space debris classification, namely characteristic length, and average cross-section, they do not consider shape characteristics vital in calculating collision damage, such as ballistic coefficient. This proposal aims to improve state-of-the-art through advanced object classification with the following methodology that utilizes 3D scanning and Machine Learning. First, high-resolution 3D scans of satellite debris are acquired imported in CATIA, and solid models from them are created from which an abundance of shape characteristics factors can be obtained. 3D scanning is followed by employing Machine Learning techniques for reducing the dimensionality of the obtained features (via the Principal Component Analysis) and replacing the current decision trees with more reliable clustering algorithms, such as K-Means Clustering or Artificial Neural Networks. Then this data is processed with Ballistic coefficient and other essential factors in the collision of a satellite with different classes of space debris found from machine learning will be calculated, and further refinement in these clusters will be investigated. 

## Project Description

### Project Background
With the current accelerating rate of satellites accruing in orbit, collisions are becoming exponentially more likely. Low earth orbit (LEO) suffers the most from Orbital Debris since it is a highly desired altitude, has the least room for satellites, and must be passed through to reach higher altitudes. In addition, stage separations to reach higher orbits may happen in LEO, meaning almost every mission into space creates orbital debris in this critical region. Combined, these factors mean that a cascade event is becoming more likely, and this critical orbital regime is on track to become too hazardous for any level of use. Due to limited tracking technologies, especially for high-altitude orbits, hazard estimation of orbital debris is only possible by utilizing a model that considers the physical characteristics of debris.
The current authority on debris modeling comes from the NASA DebriSat program. This program creates mockup satellites that are destroyed using hypervelocity impacts. The debris from the impacts is then collected and tabulated to characterize the orbital debris distribution created from an impact in space. This creates a valuable model for knowing the general makeup of orbital debris but provides insufficient information for more advanced modeling. The methods used by the DebriSat program to characterize their debris involve a purpose-built imaging system that determines the characteristic length of a piece of debris [@moraguez2015imaging; @cowardin_optical_2020] which is tabulated with the material and weight of each piece of debris. Then a simple decision tree is used to characterize the debris. Decision trees are a helpful method to categorize data. We propose to use a more advanced approach to improve the cluster characterization of the debris. 

### Objectives

The materials and construction methods used to create satellites are changing rapidly because the price to orbit is now low enough that new companies can create satellites with unproven designs, and established players can take risks and relax TRL requirements [@orbital_debris_research_and_developmentinteragency_working_group_2021_2021;@buchs2021intensifying]. However, rapid innovation in this field has the drawback that to keep their models up to date, DebriSat needs to continuously create mock satellites that reflect the cutting edge of satellite manufacturing so that their model can stay relevant. In addition, each test creates tens of thousands of pieces that must be accurately cataloged [@carrasquilla_debrisat_2019], so any new characterization method needs to be fast enough to keep up with changing satellite characteristics. It also means that the essential characteristics of orbital debris may change, so new data must be analyzed and incorporated into models. Another benefit to our approach is that since all the debris we catalog will have a 3D model saved and could potentially be reprocessed in code in the future if new modeling methods are adopted.

### Project Approach

Below in @fig-comparison, you can see an example of the current decision tree method used by DebriSat alongside the advanced 3D scan data science pipeline that we propose. Our method utilizes complex analysis only possible with a high-resolution scan of the models and then uses a variety of machine learning and data science techniques to process the data into useful metrics. Our method is a modern approach that can eventually be developed into complex simulations of debris.

![DebriSat Approach versus Our Approach](uripropcomparison.svg){#fig-comparison fig-alt="A flowchart comparing two approaches to debris satellite analysis. The Traditional Approach begins with inspecting the object and determining if it is flexible. If yes, it is labeled FLEXIBLE. If not, it is checked if one axis is significantly longer than others. If yes, it is labeled Bent Plate. If not, the process continues to other classifications. The Our Approach imports scanned geometry into MATLAB, derives data such as moment of inertia, center of mass, aerodynamic drag, density, and material from the 3D scans, and processes the data in a machine learning pipeline to determine impact lethality, accurate orbit propagation, and predictions for future impacts."}

Enough samples to get the project started have been provided by Dr. Madler, but as of now, they are entirely uncharacterized. The first step towards characterizing the debris we have is to manually organize them into different clusters. The clusters are based on similar characteristics that can be observed visually to produce a preliminary characterization and are just meant to be a starting point for the MATLAB code. Then three to five samples from each cluster will be scanned to give a somewhat even distribution of what we expect MATLAB to provide for each cluster. When clustering using machine learning methods, every cluster must have a few pieces to ensure minimal outliers in the data. As more data becomes available, the machine learning methods get more powerful. Before being put into MATLAB, every scan will be uploaded into CATIA to take data from the scans and clean up the model. CATIA makes some of the desired characteristics of the debris samples, such as the moment of inertia, the center of gravity, etc., very easy to collect. Future iterations of this project will likely do all processing in MATLAB to reduce the manual labor required for each piece of debris. 
Below in @fig-debris is a render of a real piece of debris scanned by the Rapid Prototyping Lab on campus. Even after reducing the number of points provided by the scanner, the final model has over 1.2 million points, which is an impressive resolution given that the model is only a few inches in length on its longest axis. With debris created by hyper-velocity impacts having such complex shapes, it becomes clear almost immediately that the geometry is far too complex for any sort of meaningful characterization by a human without machine learning techniques. This issue is compounded by the fact that satellites comprise many exotic materials. The DebriSat program uses a simplified satellite to reduce costs, and it still comprises 14 different categories of materials where a category is primarily a way to determine how dense the material is [@cowardin2019updates] and not for each unique material. This also means that the shapes vary wildly since PCBs, wires, batteries, and the aluminum structure reacts entirely differently to a hypervelocity collision. The example in @fig-debris, and every piece of debris we have at our disposal, are from a hypervelocity impact involving aluminum sheet metal. A dataset of one material type is beneficial at this point since our dataset is still small; it makes sense to start our characterization with a single type of debris. 

![Sample Debris Scans](long.png){#fig-debris fig-alt="A 3d renderr of a thin piece of metal with a lot of bends."}

Our data collection process gives us much more data than the traditional methods, so machine learning is required to make sense of the data. The first step towards processing our data once it has been tabulated into MATLAB is to perform a principal component analysis (PCA). Utilizing PCA has two significant benefits at this stage of the project in that it reduces the required size of our dataset and decreases the amount of computational power to process the dataset. Reducing our dataset’s dimensionality will allow us to derive what aspects of the orbital debris are truly important for the classification. This may be easy for a human to discern at this stage of the project, but the DebriSat database has almost 200,000 pieces of debris cataloged [@carrasquilla_debrisat_2019], so it is essential to start with an approach that is adaptable to big data and is robust enough to handle the metrics we are trying to classify that are very complex. Once PCA has reduced the dataset, it can be clustered using the k-means method. K-means is a method of categorizing large, complex datasets using pattern recognition. Depending on which insight we are looking for, k-means could produce a valuable result, or it could be a step to much more advanced machine learning methods of analysis.

## Safety Considerations

As with any research, safety is the number one priority, and steps need to be taken to ensure that every step of the project is approached as safely as possible. Lucky for this project, the only safety consideration is that the debris pieces can have sharp edges and need to be handled with care. Anyone interacting with the debris will be informed of this risk, and the debris will only be handled in the RPL lab where it is safely stored, and the room also has a first aid kit. That said, the risk of a cut from the debris is very low, and the severity would likely be on the magnitude of a paper cut. 

## Outreach Activities

Outreach will be performed at the Prescott Regional SciTech Festival, which Embry-Riddle holds on March 5th, but this year will be held virtually. This is an excellent opportunity to share our research with professionals across the STEAM community. 
Expected Project Outcomes
Orbital debris is pollution and is on a visible path towards reaching a point of no return where our access to space could be completely cut off for decades. Even a reduction in space activities could have a long-standing impact on military, commercial and civilian operations [@buchs2021intensifying]. This research will increase our understanding of the current orbital debris environment and enable further research to mitigate new debris by creating more advanced collision avoidance and manufacturing techniques for rockets and satellites that decrease the amount of debris generated. 
The student researchers will also benefit from performing this project. First, students will learn how to collect a large dataset specifically to make it accessible by the code they write. Then a great deal of work will be spent on analyzing the data, which will rely on knowledge of solid mechanics, space mechanics, materials science, and machine learning to characterize the debris properly.

