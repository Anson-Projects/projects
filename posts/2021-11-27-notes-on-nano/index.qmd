---
title: "Notes on Nano"
description: |
  Nano (Ӿ) is a fast, feeless and severely underrated currency.
description-meta: |
  Explore Nano (XNO), a fast and feeless cryptocurrency. This post examines Nano's unique block-lattice architecture, its potential as a true digital currency, and compares it to other cryptocurrencies like Bitcoin, Ethereum, and Ripple. Learn about Nano's advantages, challenges, and its potential for mainstream adoption. Includes data visualizations and links to further resources.
date: 2021-12-08
date-modified: 2024-02-29
categories:
  - Crypto
  - Notes
  - Tech Breakdown
bibliography: citations.bib
creative_commons: CC BY
image: banner.jpg
image-alt: A blue Nano symbol Ӿ (x with a bar across the midle) superimposed on an abstract background of interconnected lines, resembling a network or web.
---

## The Current State of Crypto

In 2021 news about cryptocurrency has been dominated by _meme coins_. It is without a doubt that the crypto bull runs of 2021 have been thanks to the hype around coins with zero utility besides the ability to rapidly shoot up in value due to carefully constructed tokenomics^[@Shiba]. Unfortunately, the hype surrounding coins with actual utility has been disappointingly low in 2021.

The current narrative of the most influential coins is that Bitcoin will change financial infrastructure and keep governments from taking our freedom. Ethereum will be the backbone for everything from the internet, to art, to the mortgage on your house. The rest of the top 10 (and most of the top 100) coins by market cap are either smart-contract platforms with little community gunning for Ethereum's spot or meme coins promising valuations that challenge the wealth of the economy of the world.

## Return of the Utility

The coin I want to talk about is Nano (XNO). As of writing, it is ranked #175 by market capitalization. Nano does one thing and executes it perfectly: being a currency. The current market capitalization is far lower than other coins with similar amounts of development and community. Ethereum has a vibrant ecosystem where usage causes more usage. [The figure here illustrates this nicely.](https://twitter.com/domothy/status/1462988092848156677/photo/1) The only way to increase demand for Nano and increase the price is to get it used for everyday transactions which would make the limited supply more scarce. Currencies have so much more to offer the world than just stores of value that can shoot up in price, and I think Nano is the perfect example of that. A coin of pure utility.

<!-- ![Data from [@kraken_deposit] and [@coingecko]](caps.svg) -->

:::{.column-body-outset}
<figure>
<embed
  type="text/html"
  src="caps.html"
  width="100%"
  style="height: 60vh"
/>
<figcaption>Data from [@kraken_deposit] and [@coingecko]</figcaption>
</figure>
:::


Transactions must happen in seconds for a currency to be usable. Above are all the coins listed on Kraken, along with their confirmation time. Confirmation time is the amount of time Kraken recommends waiting before a transaction can be trusted not to be reversed on the blockchain. Kraken's times are conservative, but I believe them to be a trusted authority on the topic. It's important to note that most of the coins on this list are not designed with buying coffee in mind, but I think the comparison is still relevant since it is commonly believed that the only thing holding Bitcoin and its derivatives back from being used everywhere is hitting a critical mass of adoption among retail. Bitcoin is making progress, but Bitcoin today is far from being used as a currency.

![Data from [@coingecko]](fast.svg){alt-text="A bar chart titled "Market Caps of Fast Coins" showing the market capitalization of various cryptocurrencies. The x-axis lists the cryptocurrencies ripple, nano, serum, raydium, oxygen, kava, stellar, icon, cosmos, and solana. The y-axis represents market cap in billions of USD. Each bar is colored according to the cryptocurrency's category: Currency, DeFi, Parachain, and Smart Contract. Solana has the largest market cap, followed by ripple."}

Above are all of the coins capable of settling in less than 1 second, which is about on par with a credit card. I also believe that confirming in less than a second is a benchmark that must be hit to be useful for everyday transactions. Solana stands out the most since it is the only smart contract platform on the list and has the highest market cap. Solana has a ton of potential, but I don't believe that a coin with a smart-contract platform can compete long-term with a coin specifically designed to be a currency. The DeFi and Parachain coins are more governance tokens than actual cryptocurrencies, so they aren't designed with the scale required for use as a currency. That leaves Ripple and Nano, which have wildly different market caps. Ripple is quite controversial since it is centralized to the point that I would argue it's closer to Visa than Bitcoin^[@Jain_2021]. I believe a fraudulent organization runs Ripple, so I don't want to justify it by talking about it too much here. However, it would be biased not to compare the performance and tokenomics since they are the only two serious projects with the goal to become a currency.

:::{.column-body-outset}
| | Nano^[@nano] | Ripple^[@ripple] |
| ----------------------------------------------- | ------------------------------------------------------------ | ----------------------------------------------------------------------------------------- |
| Ticker | XNO | XRP |
| Symbol | Ӿ | N/A |
| Market Cap^[@coingecko] | .650B | 46B |
| Transaction Speed | <1 second | 3+ seconds |
| Transaction Fee | 0 | 0.00001 XRP^[Subject to change at any moment.] |
| Minimum Nakamoto Coefficient^[@Srinivasan_2017] | 10^[@NLReps] | 1 |
| Supply | All coins minted, no mechanism to remove coins from network. | All coins minted, transactions reduce overall supply. |
| Foundation Ownership | Ӿ7 million Developer Fund | Ripple foundation and the founder own well over half of all the coins.^[cryptonomist_xrp] |
| Initial Distribution | Faucets | Initial Coin Offering |
:::

## What is Nano

**Nano is fast** because it uses a block-lattice architecture. This means that there is no concept of block times which are the limiting factor for most cryptocurrency's transaction time. Every wallet has a unique blockchain, and each "block" only contains a single transaction. Since the block is only needed when a transaction is made, wallets can pre-mine a block and hold onto the proof of work. Since each wallet manages its blockchain and proof of work, nodes only need to worry about the balance of each wallet. This enables transactions to be instantly propagated to the network, and the nodes that govern the network only keep track of the balance of each wallet.

**Nano is free** to transact because it does not rely on paying miners for proof of work, and it is designed to be efficient so running a node is cheap and does not require support from the network. Bitcoin requires 1900 kWh of power for a single transaction^[@BTC_ECI], whereas Nano is only 0.111 Wh per transaction^[CanadianVelociraptor_2018] which is a difference of about 17 million percent.^[Numbers as of writing. Mining difficulty is variable, and hardware efficiency gets better every day.] There are many arguments that no one will run the nodes required to keep the Nano network healthy without proper incentives. Nano can work around this by keeping the requirements to run a Node very low so that the indirect benefits of running a node far outweigh the costs of the hardware. Companies will likely run nodes since running a node will be cheaper than paying transaction fees for other payment methods. The Nano whitepaper goes into great detail about the types of nodes.

## Why isn't Nano Mainstream

Most cryptocurrencies have features that directly benefit other users or network maintainers from being used. Transaction fees fed back into the coin's ecosystem are a great way to create feedback loops that generate more use for a coin. The incentive structure for Nano is much less direct but arguably much more potent. Established players in the cryptocurrency and payment processor world benefit significantly from having transactions go through them. To single out Coinbase, they have a feature where you can transfer crypto between any other member on the platform instantly and free^[@CoinbaseHowSend]. This proprietary feature could create a walled garden that Nano threatens to dismantle.

It would be unfair to suggest that Nano's problems are from big established players fighting innovation. Nano's most vital features also make it susceptible to spam attacks. Since there is no fee and the network supports dividing 1 Nano into 30 decimal places, attackers can spam the network by sending trace amounts of Nano to random addresses. The ability to send these dust transactions creates a ton of stress on the network and has even ground Nano to a halt in the past^[@harperNanoNetworkFlooded2021]. However, the network did perform better than most had hoped, and the spam attack worked as an excellent verification of Nano's capabilities. Since then, the development team has focused on solutions to ensure that the issue doesn't occur again. It is worth noting that although Nano is feeless, it is not free. So, while sending Nano does not cost any Nano, it does cost computation, so attacks on the network are still costly.

---

I'll close this off by saying that I highly recommend any cryptocurrency enthusiast to please read the [Nano Whitepaper](https://content.nano.org/whitepaper/Nano_Whitepaper_en.pdf) since it is one of the best cryptocurrency whitepapers, and in my opinion, second only to the original [Bitcoin Whitepaper](https://bitcoin.org/en/bitcoin-paper). I hope that this article was insightful, and even though I am a holder of Nano, I hope that the information is unbiased. If you think Nano is worth checking out, the Nano Foundation has an excellent getting started page: https://nano.org/try-nano
