using PlotlyJS
using CSV
using HTTP
using DataFrames
using StringViews
using JSON
using Dates

kraken = """
cryptocurrency	confirmationMinutes
0x (ZRX)	5
1inch (1INCH)	5
Aave (AAVE)	5
Aavegotchi (GHST)	5
Algorand (ALGO)	0.75
Ankr (ANKR)	5
Aragon (ANT)	5
Augur (REP)	5
Augur v2 (REPV2)	5
Axie Infinity (AXS)	5
Badger DAO (BADGER)	5
Balancer (BAL)	5
Bancor (BNT)	5
Band Protocol (BAND)	5
Basic Attention Token (BAT)	5
Bifrost (BNC)	4
Bitcoin (BTC)	40
Cardano (ADA)	10
Cartesi (CTSI)	5
Chainlink (LINK)	5
Chiliz (CHZ)	5
Compound (COMP)	5
Cosmos (ATOM)	0
Covalent (CQT)	5
Curve (CRV)	5
Dash (DASH)	5
Dai (DAI) ERC-20	5
Decentraland (MANA)	5
Dogecoin (DOGE)	40
dYdX (DYDX)	5
Energy Web Token (EWT)	1.75
Enjin Coin (ENJ)	5
Enzyme Finance (MLN)	5
EOS (EOS)	0
Ethereum (ETH)	5
Flow (FLOW)	1
Gnosis (GNO)	5
ICON (ICX)	0
Injective Protocol (INJ)	5
Karura (KAR)	4
Kava (KAVA)	0
Keep Network (KEEP)	5
Kusama (KSM)	2
Kyber Network (KNC)	5
Lisk (LSK)	51
Litecoin (LTC)	30
Livepeer (LPT)	5
Loopring (LRC)	5
Maker (MKR)	5
Mina (MINA)	60
Mirror Protocol (MIR)	5
Monero (XMR)	30
Moonriver (MOVR)	4
Nano (XNO)	0
Ocean (OCEAN)	5
OmiseGO (OMG)	5
Orchid (OXT)	5
Origin Protocol (OGN)	5
Oxygen (OXY)	0
PAX Gold (PAXG)	5
Perpetual Protocol (PERP)	5
Phala (PHA)	5
Polkadot (DOT)	2
Polygon (MATIC)	5
Qtum (QTUM)	60
Rarible (RARI)	5
Raydium (RAY)	0
REN Protocol (REN)	5
Ripple (XRP)	0
Serum (SRM)	0
Shiden (SDN)	4
Siacoin (SC)	60
Solana (SOL)	0
Stellar Lumens (XLM)	0
Storj (STORJ)	5
Sushi (SUSHI)	5
Synthetix (SNX)	5
tBTC (TBTC)	5
Tether USD (USDT) ERC-20	5
Tezos (XTZ)	15
The Graph (GRT)	5
The Sandbox (SAND)	5
Tron (TRX)	1
Uniswap (UNI)	5
USD Coin (USDC)	5
Waves (WAVES)	10
Wrapped Bitcoin (WBTC)	5
Yearn Finance (YFI)	5
Zcash (ZEC)	60
"""

df = CSV.File(IOBuffer(kraken)) |> DataFrame

df.symbol = [split(split(sym, "(")[end], ")")[1] |> lowercase for sym in df.cryptocurrency]

coins = HTTP.get("https://api.coingecko.com/api/v3/coins/list").body |>
        StringView |> JSON.parse .|> DataFrame |> x -> vcat(x...)

coins = coins[coins.id.!="xeno-token", :]

coins = unique(coins[end:-1:1, :], :symbol);

df = innerjoin(df, coins, on = :symbol)

mktcap_url = "https://api.coingecko.com/api/v3/simple/price?vs_currencies=usd&include_market_cap=true&include_24hr_vol=true&ids=" * join(df.id, ",")

caps = HTTP.get(mktcap_url).body |>
       StringView |> JSON.parse

df.mktcap = [caps[id]["usd_market_cap"] for id in df.id]
df.volume = [caps[id]["usd_24h_vol"] for id in df.id]
df.logvol = log.(df.volume)
df = df[df.mktcap.!=0, :]



p = plot(df,
    x = :confirmationMinutes,
    y = :mktcap, mode = "markers",
    text = :cryptocurrency,
    marker_color = :logvol,
    marker = attr(colorscale = "Viridis", line = attr(width = 0.4, color = "DarkSlateGrey")),
    Layout(title = "Market Cap vs. Confirmation Time, Data From: $(today())",),
    labels = Dict(
        :confirmationMinutes => "Confirmation Time (Minutes)",
        :mktcap => "Market Capitalization  (USD)"),
)

open("./caps.html", "w") do io
    PlotlyBase.to_html(io, p.plot)
end