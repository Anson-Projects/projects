---
title: "How to Export Fusion 360 files to Other File Types"
description: |
  Export your Fusion 360 designs with ease. Learn how to access hidden export options and convert your models to various file types, including STL, OBJ, STEP, and more. Discover which format is best for 3D printing, CAD compatibility, and other uses.
date: 2018-09-12
date-modified: 2024-02-29
categories:
  - 3D Modeling
  - Notes
creative_commons: CC BY
image: banner.png
image-alt: A slide showing the Fusion 360 logo on the left and a cloud icon on the right, with double arrows between them indicating data transfer in both directions.
---


A major downfall of Fusion 360 is its lack of exporting options and its overall lack of interoperability with other CAD software. Its main filetype `.f3d`, seems to be closed source and isn't supported by any other programs. A naive approach of exporting to an intermediary format like `.step` or `.obj` then using something else like Autodesk Inventor or Blender to export to your final filetype from is likely to cause artifacts, loss of dimensions, and be a slow manual process. Luckily Fusion has more export options built-in, but they don't show the functionality in their desktop program.

## [Go to the Autodesk 360 Dashboard](https://myhub.autodesk360.com)

Going to [https://myhub.autodesk360.com](https://myhub.autodesk360.com) will give you a similar looking Project and file options as the data panel in the Desktop version. Navigate to your model and open it. Whether you are in the Design View or the 3D View in the top right corner, you'll have a few options to choose from.

Clicking the download button provides you with a ton more options than the desktop version offers. Unfortunately, you cannot download straight from the website as it takes time to convert, so you have to wait for an email. In my experience, this usually takes less than a minute although I would imagine more complex models take more time.

Below I have a list of every file-type that can be exported and a quick summary of what they are good for.

![List of Export Options](websiteExport.png){fig-alt="A screenshot of the export options in Fusion 360. The options are Fusion 360 Archive, Inventor 2016, IGES, SAT, SMT, STEP, DWG, DXF, STL, FBX, SketchUp, and OBJ."}

## Fusion 360 Export Formats

The file extension doesn't give much information about what a format actually is so I aim to give a brief description of each export format below. Basic rule of thumb as of writing is to export as an `.stl` if your 3D printing, and `.obj` if your going to a different piece of software to work on the model.

:::{.column-body-outset}
| FORMAT | WHAT ITS GOOD FOR. |
|--------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Fusion 360 Archive | Provides a `.f3d` file-type which is a proprietary file-type for Fusion 360. Preserves full model functionality when opened in any Fusion 360, and is a great way to make personal backups of Fusion Models. |
| Inventor 2016 | Downloads either a single `.ipt` if the part is a single component, or multiple `.ipt` files and a `.iam` if your Fusion model contains multiple components. `.ipt` is an Inventor Part file, and `.iam` is an Inventor Assembly file. The models lose all parametric abilities, but keep dimensions and shape. |
| IGES | `.iges` is an open CAD format and should be usable in any CAD software. Does not retain any parametric elements |
| SAT | Developed by Dassault Systems but is a pretty widely accepted file-type for CAD software. Does not retain any parametric elements. |
| SMT | Only usable by Fusion 360 and Inventor 2019 |
| STEP | `.step` is an open CAD format and should be usable in any CAD software. Does not retain any parametric elements |
| DWG | Autodesk AutoCAD file format. Exports a wireframe of your model. Great for exporting to AutoCAd but otherwise useless. |
| DXF | Drawing format. Exports a wireframe of your model. Much better off making an actual drawing in fusion then exporting a `.dxf` |
| STL | `.stl` is an open standard used by most 3D modeling and CAD programs and is great for 3D printing, keep in mind that units are not stored in `.stl` formats, so it is important to remember what units your model was created in. If you aren't prompted by the soft then it likely assumed millimeters so you may need to convert yourself by scaling the model. |
| FBX | Autodesk owned file format. No real use in CAD and doesn't preserve dimensions so you end up with a tiny model. |
| SketchUp | Exports a `.skp` which opens in SketchUp perfectly, Great if you want to use your part in a SketchUp Project. |
| OBJ | `.obj` or Object is an open format and the geometry should be workable in most 3D modeling or CAD programs. |
:::

_If you have anything to add to this table please contact me._
