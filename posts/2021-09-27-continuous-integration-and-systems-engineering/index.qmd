---
title: "Continuous Integration and Systems Engineering"
description: |
  Streamline systems engineering projects with Continuous Integration (CI). This post explains how CI can generate automatic reports, keeping teams informed on complex projects. See a real-world example of how a student capstone team used CI to bridge the gap between software and systems engineering, improving communication and progress tracking. Includes a link to the project repository.
date: 2021-10-04
date-modified: 2024-02-29
categories:
  - Systems Engineering
  - Capstone
  - University
  - Code
  - Python
  - CI/CD
---

## The Problem

Working on a complex project that spans multiple domains can be challenging for a small team. In addition, the small team size encourages every team member to keep entirely up to date on the inner workings of every system, but that can add a ton of overhead and make progress slow to a halt.

<aside>Teammates can also slow meetings to a halt or make low quality project contributions if tasked to contribute on a subsystem they aren't knowledgeable of.</aside>

My [Capstone](https://projects.ansonbiggs.com/#category:Capstone) team makes this even harder since all, but one of us are Astronautical Engineers. Astronautical is just a fancy way of saying, "Systems, but every problem is a satellite" so a project like ours with a massive Embedded software aspect can be challenging since the most crucial part of the project is essentially an enigma to a majority of the team.

So what we needed was a way for me to write code and to keep the rest of the team up to date without ever having to touch the code and ideally without me having to generate any kind of report manually.

## The Solution

With Continuous Integration and a little Python, my GitLab repository automatically builds the latest code, runs the code, produces plots with the results, and makes an executable available if someone is inclined to run the code themselves. The automation means that the project readme is always up to date, and if anyone is wondering how progress is proceeding, they can check the readme and view the current performance in an accessible format.

The data availability is also excellent for when my team has to produce a progress report because there is never waiting on the person who can run the code and then build some plots. Unfortunately, the same can't be said for the other sub-teams who usually have difficulty compiling their progress into a report.

## The Result

All of the code being used for the Capstone is public, and the specific repository that I've been talking about can be accessed here: https://gitlab.com/lander-team/lander-cpp As you can see, the readme has charts that are always up to date. The `.gitlab-ci.yml` file contains all the build info, and you'll likely be surprised how simple of a setup it is.
