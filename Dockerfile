FROM ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive

ENV JULIA_VERSION=1.11.1 \
    JULIA_MAJOR_VERSION=1.11 \
    JULIA_PATH=/usr/local/julia \
    QUARTO_VERSION=1.6.37

RUN apt-get update && apt-get install -y --no-install-recommends \
    apt-utils dialog \
    git iproute2 procps lsb-release \
    python3 python3-pip python3-dev \
    r-base \
    gcc g++ \
    wget curl tar \
    openssh-client \
    && rm -rf /var/lib/apt/lists/*

# Use a RUN command for architecture detection and conditional logic
RUN wget https://github.com/quarto-dev/quarto-cli/releases/download/v${QUARTO_VERSION}/quarto-${QUARTO_VERSION}-linux-$(if [ "$(uname -m)" = "x86_64" ]; then echo "amd64"; else echo "arm64"; fi).tar.gz -O quarto.tar.gz \
    && tar -xzf quarto.tar.gz -C /opt \
    && mkdir -p /opt/quarto \
    && mv /opt/quarto-${QUARTO_VERSION}/* /opt/quarto/ \
    && ln -s /opt/quarto/bin/quarto /usr/local/bin/quarto \
    && rm -rf quarto.tar.gz /opt/quarto-${QUARTO_VERSION}

RUN python3 -m pip install jupyter webio_jupyter_extension jupyter-cache

RUN curl -fsSL "https://julialang-s3.julialang.org/bin/linux/$(if [ "$(uname -m)" = "x86_64" ]; then echo "x64"; else echo "aarch64"; fi)/${JULIA_MAJOR_VERSION}/julia-${JULIA_VERSION}-linux-$(if [ "$(uname -m)" = "x86_64" ]; then echo "x86_64"; else echo "aarch64"; fi).tar.gz" -o julia.tar.gz \
    && tar -xzf julia.tar.gz -C /tmp \
    && mv /tmp/julia-${JULIA_VERSION} ${JULIA_PATH} \
    && rm -rf julia.tar.gz /tmp/julia-${JULIA_VERSION} \
    && ln -s ${JULIA_PATH}/bin/julia /usr/local/bin/julia

COPY Project.toml /root/.julia/environments/v${JULIA_MAJOR_VERSION}/
COPY Manifest.toml /root/.julia/environments/v${JULIA_MAJOR_VERSION}/

RUN julia -e "using Pkg; Pkg.instantiate(); Pkg.precompile()"

ENV QUARTO_DENO_V8_OPTIONS=--stack-size=10000